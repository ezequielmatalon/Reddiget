package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import helper.Holder;
import helper.Utils;
import model.Post;
import reddit.deviget.ezequiel.reddit.R;

/**
 * Created by Ezequiel on 14/10/2014.
 */
public class Adapter extends ArrayAdapter<Post> {
    private final Context context;
    private final List<Post> values;
    public Boolean loaded;
    Holder holder;

    public Adapter(Context context,List<Post> values) {
        super(context, R.layout.activity_my, values);
        this.context = context;
        this.values = values;
        loaded = false;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

           LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            convertView = inflater.inflate(R.layout.listlayout, parent, false);
            TextView textView1 = (TextView) convertView.findViewById(R.id.firstLine);
            TextView textView2 = (TextView) convertView.findViewById(R.id.secondLine);
             ImageView imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
             Post post = values.get(position);

            String imageUrl = post.getUrl();

            holder = new Holder();
            holder.post=post;
            holder.thumb=imageView;
            holder.position=position;

        AsyncTask<Holder, Void, Bitmap> execute = new AsyncTask<Holder, Void, Bitmap>() {
            private Holder holder;

            @Override
            protected Bitmap doInBackground(Holder... params) {
                holder = params[0];


                Bitmap bitmap = null;
                if(holder.post.hasThumbnail()){
                    bitmap= Utils.downloadBitmap(holder.post.getThumbnail());
                }




                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);
                if (holder.position == position) {
                    // If this item hasn't been recycled already, hide the
                    // progress and set and           try {
                    holder.thumb.setVisibility(View.VISIBLE);
                    holder.thumb.setImageBitmap(result);
                }
            }


        };
        execute.execute(holder);
/*
            if (post.hasThumbnail()) {
                ExecutorService executor = Executors.newSingleThreadExecutor();
                Future<Bitmap> future = executor.submit(new Callable<Bitmap>() {
                    @Override
                    public Bitmap call() throws Exception {
                        Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(post.getThumbnail()).getContent());
                        if (bitmap != null) {
                            bitmap = Bitmap.createScaledBitmap(bitmap, 40, 40, false);
                        }
                        return bitmap;
                    }
                });

                try {
                    if (future.get() != null) {

                        imageView.setImageBitmap(future.get());
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                imageView.setImageBitmap(null);
            }
*/
            textView1.setText(post.getTitle());
            textView2.setText("Author: " + post.getAuthor() + " - " + "No. of comments: " + post.getNumComments() + " - " + post.getTimeAgo());


        return convertView;
    }

    public void addData(Vector<Post> data1) {
       this.addAll(data1);

    }


    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


}