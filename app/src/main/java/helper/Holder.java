package helper;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import model.Post;

/**
 * Created by Ezequiel on 17/10/2014.
 */
public class Holder {

   public ImageView thumb;
    public Post post;
    public int position;
}