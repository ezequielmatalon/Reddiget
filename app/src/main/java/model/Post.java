package model;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import helper.Utils;

/**
 * Created by Ezequiel on 14/10/2014.
 */
public class Post implements Serializable {

    private static final long serialVersionUID = 1L;

    @Expose
    private String title;

    @SerializedName("created_utc")
    @Expose
    private Long createdUtc;

    @Expose
    private String author;

    @SerializedName("num_comments")
    @Expose
    private Integer numComments;

    @Expose
    private String thumbnail;

    @Expose
    private String url;

  /*  private Bitmap thumbnailImage;
*/
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
           }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DateTime getCreatedUtc() {
        return new DateTime(createdUtc * 1000, DateTimeZone.UTC);
    }

    public void setCreatedUtc(Long createdUtc) {
        this.createdUtc = createdUtc;
    }

    public Integer getNumComments() {
        return numComments;
    }

    public void setNumComments(Integer numComments) {
        this.numComments = numComments;
    }

    public String getTimeAgo() {
        Period p = new Period(getCreatedUtc(), new DateTime(DateTimeZone.UTC));
        String timeBetween;
        Integer hours = p.getHours();
        Integer minutes = p.getMinutes();
        Integer days = p.getDays();
        Integer weeks = p.getWeeks();
        Integer years = p.getYears();

        if (years > 0) {
            timeBetween = years + " week" + (years < 2 ? "" : "s") + " ago";
            // } else if (weeks > 0) {
            // timeBetween = weeks + " week" + (weeks < 2 ? "" : "s")
            // + " ago";
        } else if (days > 0) {
            timeBetween = days + (weeks * 7) + " day"
                    + (days + (weeks * 7) < 2 ? "" : "s") + " ago";
        } else if (hours > 0) {

            timeBetween = hours + " hour" + (hours < 2 ? "" : "s") + " ago";
        } else {
            timeBetween = minutes + " minute"
                    + (minutes < 2 && minutes > 0 ? "" : "s") + " ago";

        }
        return timeBetween;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean hasImage()  {
        Boolean image = false;
        if (StringUtils.isNotEmpty(url)) {
            try{
                URLConnection connection = new URL(url).openConnection();
                String contentType = connection.getHeaderField("Content-Type");
                image = contentType.startsWith("image/");
            }catch(Exception e){
                return false;
            }
        }
        return image;
    }

    public Boolean hasThumbnail(){
        return StringUtils.isNotEmpty(thumbnail);
    }
/*
    public Bitmap getThumbnailImage(){
        return thumbnailImage;
    }
*/
    public Bitmap getUrlImage(){
        return hasImage()?getImageFromUrl(url):null;

    }

    private Bitmap getImageFromUrl(String url){
        Bitmap bitmap= null;
        if(hasImage()) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }
}
