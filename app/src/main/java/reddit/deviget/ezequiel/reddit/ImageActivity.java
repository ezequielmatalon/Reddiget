package reddit.deviget.ezequiel.reddit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import reddit.deviget.ezequiel.reddit.R;
import model.Post;

/**
 * Created by Ezequiel on 14/10/2014.
 */
public class ImageActivity extends Activity{

    private static Bitmap image;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image);
        Bundle extras = getIntent().getExtras();
        Post post=(Post) extras.get("post");

         if(post.hasImage()){
             image=post.getUrlImage();
             ImageView img = (ImageView) this.findViewById(R.id.imageView);
             Button but = (Button) this.findViewById(R.id.button);
            but.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     try {
                         saveFile(image);
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
             });
             img.setImageBitmap(image);
             img.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     try {
                         loadGallery();
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 }
             });
         }else{
             String url = "http://www.example.com";
             Intent i = new Intent(Intent.ACTION_VIEW);
             i.setData(Uri.parse(post.getUrl()));
             startActivity(i);
         }

    }

    public File saveFile(Bitmap bmp) throws IOException {
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Reddit";
        File dir = new File(file_path);
        if(!dir.exists()){
            dir.mkdirs();
        }

        File file = new File(dir, "image" + new Date().toString() + ".png");
        FileOutputStream fOut = new FileOutputStream(file);


        bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        fOut.flush();
        fOut.close();
        return file;
    }


    public void loadGallery() throws Exception {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(getImageUri(getBaseContext(), image), "image/*");
        startActivity(intent);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
