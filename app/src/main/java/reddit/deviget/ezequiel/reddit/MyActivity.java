package reddit.deviget.ezequiel.reddit;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;


import org.apache.commons.httpclient.HttpMethod;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


import adapter.Adapter;
import model.Post;


public class MyActivity extends ListActivity {

    private static Adapter adapter;
    private static final Integer pageSize = 10;
    private static Vector<Post> currentList;
    private static Integer currentPage;
    private static final String urlREST = "http://www.reddit.com";
    private static Boolean everythingLoaded = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paginationStrategy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(this, ImageActivity.class);

        intent.putExtra("post", currentList.get(position));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Vector<Post> getFiftyTopResults(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new NameValuePair("limit","50"));
        return getTopResults(params);
    }

    private Vector<Post> getTopResults(List<NameValuePair> params) {
        Vector<Post> posts = new Vector<Post>();
        try {


            String json = doRequest(params,"/top.json","GET");
            Gson gson = new Gson();
            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(json).getAsJsonObject();


            JsonObject data = object.getAsJsonObject("data");
            JsonArray array = data.getAsJsonArray("children");



            for (JsonElement obj : array) {
                posts.add(gson.fromJson(
                        obj.getAsJsonObject().getAsJsonObject("data"),
                        Post.class));
            }
        }catch (Exception e){

        }
        return posts;

    }

    private Vector<Post> getPage(Integer page){
        Integer finalElement = null;
        if(page*pageSize>currentList.size()){
            finalElement = currentList.size();
        }else{
           finalElement = (pageSize*page);
        }


        Vector vector = new Vector<Post>(currentList.subList(pageSize*(page-1),finalElement));
        return vector;
    }


    private String doRequest(List<NameValuePair> params, String endpoint, String method){

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        StringBuilder sb = new StringBuilder();

        for (NameValuePair param:params){
            if(sb.length()>0){
                sb.append("&");
            }
            sb.append(param.getName() + "=" + param.getValue());
    }
        String parametersQuery = "?" + sb.toString();
        HttpClient client = new HttpClient();
        client.getParams().setParameter(HttpMethodParams.USER_AGENT,
                "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

        HttpMethod metha = null;
        if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
            PostMethod trans = new PostMethod(urlREST + endpoint + parametersQuery) {
                @Override
                public boolean getFollowRedirects() {
                    return true;
                }
            };
            for (NameValuePair nameValuePair : params) {
                trans.addParameter(nameValuePair);
            }
            metha = trans;

        } else {

            metha = new GetMethod(urlREST+ endpoint + parametersQuery) {
                @Override
                public boolean getFollowRedirects() {
                    return true;
                }

            };
        }

        String body = null;
        try {
            client.executeMethod(metha);

            body = metha.getResponseBodyAsString();

        } catch (Exception e) {

        } finally {
            // release any connection resources used by the method
            metha.releaseConnection();
        }

        return body;


    }

    private Integer getTotalPages(){
        if(currentList.size()%pageSize==0){
            return (currentList.size()/pageSize);
        }else{
            return (currentList.size()/pageSize)+1;
        }
    }

    private void paginationStrategy(){
        currentList = getFiftyTopResults();
        currentPage = 1;
        adapter = new Adapter(this, getPage(currentPage));
        setListAdapter(adapter);
        adapter.setNotifyOnChange(false);

        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!everythingLoaded) {
                    int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem == currentPage * pageSize) {

                        if (currentPage < getTotalPages()) {
                            currentPage++;
                            Vector<Post> page = getPage(currentPage);
                            adapter.addData(page);
                            adapter.notifyDataSetChanged();


                        }else{
                            adapter.loaded=true;
                            everythingLoaded=true;
                        }

                    }
                }
            }

        });
    }

    private void paginationStrategy2(){

    }

}
